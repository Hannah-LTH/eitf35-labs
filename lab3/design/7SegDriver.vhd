library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity seven_seg_driver is
   port ( clk           : in  std_logic;
          reset         : in  std_logic;
          BCD_digit     : in  std_logic_vector(9 downto 0);          
          sign          : in  std_logic;
          overflow      : in  std_logic;
          DIGIT_ANODE   : out std_logic_vector(3 downto 0);
          SEGMENT       : out std_logic_vector(6 downto 0)
        );
end seven_seg_driver;

architecture behavioral of seven_seg_driver is
    component binary_to_sg is
        port (
             binary_in : in std_logic_vector(3 downto 0);
             sev_seg   : out std_logic_vector(6 downto 0)
              );
    end component;
    
    signal counter16_reg, counter16_next : unsigned(15 downto 0);
    signal counter2_reg, counter2_next : unsigned(1 downto 0);
    signal counter16_overflow : std_logic;
    signal seg_bcd : std_logic_vector(3 downto 0);

begin

    binary_to_7seg : binary_to_sg
        port map (binary_in => seg_bcd, sev_seg => SEGMENT);

    process(clk, reset)
    begin
        if reset = '1' then
            counter16_reg <= (others => '0');
            counter2_reg <= (others => '0');
        elsif rising_edge(clk) then
            counter16_reg <= counter16_next;
            counter2_reg <= counter2_next;
        end if;
    end process;
    
    counter16_next <= counter16_reg + 1;
    counter2_next <= counter2_reg + 1 when (counter16_overflow = '1') else
                     counter2_reg;
    counter16_overflow <= '1' when (counter16_reg = 65535) else
                          '0';

    process(counter2_reg, BCD_digit, sign, overflow)
    begin
        case counter2_reg is
            when "00" =>
                DIGIT_ANODE <= "1110";
                seg_bcd <= BCD_digit(3 downto 0);
            when "01" =>
                DIGIT_ANODE <= "1101";
                seg_bcd <= BCD_digit(7 downto 4);
            when "10" =>
                DIGIT_ANODE <= "1011";
                seg_bcd <= "00" & BCD_digit(9 downto 8);
            when others =>
                DIGIT_ANODE <= "0111";
                if (overflow = '1') then
                    seg_bcd <= "1010";
                elsif (sign = '1') then
                    seg_bcd <= "1011";
                else
                    seg_bcd <= "1100";
                end if;
        end case;
    end process;
end behavioral;
