library ieee;
use ieee.std_logic_1164.all;

library work;
use work.ALU_components_pack.all;

entity binary2BCD is
   generic ( WIDTH : integer := 8   -- 8 bit binary to BCD
           );
   port ( binary_in : in  std_logic_vector(WIDTH-1 downto 0);  -- binary input width
          BCD_out   : out std_logic_vector(9 downto 0)        -- BCD output, 10 bits [2|4|4] to display a 3 digit BCD value when input has length 8
        );
end binary2BCD;

architecture structural of binary2BCD is 

--declaration for binary2BCD_algorithm
component binary2BCD_algorithm is
  port(  input : in std_logic_vector(17 downto 0);
          output: out std_logic_vector(17 downto 0)
         );
end component;

-- SIGNAL DEFINITIONS HERE IF NEEDED
signal temp: std_logic_vector(17 downto 0);
signal result_1,result_2,result_3,result_4,result_5,result_6,result_7: std_logic_vector(17 downto 0);

begin  

-- DEVELOPE YOUR CODE HERE

    temp<=("0000000000"&binary_in);

--use components to shift 7 times(8 times in total)
    unit1: binary2BCD_algorithm
        port map(input=>temp, output=>result_1);
    unit2: binary2BCD_algorithm
        port map(input=>result_1, output=>result_2);
    unit3: binary2BCD_algorithm
        port map(input=>result_2, output=>result_3);
    unit4: binary2BCD_algorithm
        port map(input=>result_3, output=>result_4);
    unit5: binary2BCD_algorithm
        port map(input=>result_4, output=>result_5);
    unit6: binary2BCD_algorithm
        port map(input=>result_5, output=>result_6);
    unit7: binary2BCD_algorithm
        port map(input=>result_6, output=>result_7);
        -- another shift and output BCD
        BCD_out<=result_7(16 downto 7);
end structural;



