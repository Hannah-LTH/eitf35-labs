library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sub_if_geq is
    generic (THRESHOLD : unsigned);
    port ( x_in : in std_logic_vector (7 downto 0);
           x_out : out std_logic_vector (7 downto 0)
          );
end sub_if_geq;

architecture behavioral of sub_if_geq is
    signal y : unsigned (7 downto 0);
begin
    y <= THRESHOLD when (unsigned(x_in) >= THRESHOLD) else
         to_unsigned(0, 8);
    x_out <= std_logic_vector(unsigned(x_in) - y);
end behavioral;