library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mod_3_unit is
    port ( A_in : in std_logic_vector (7 downto 0);
           signed_mode : in std_logic;
           A_mod : out std_logic_vector (7 downto 0)
          );
end mod_3_unit;

architecture behavioral of mod_3_unit is
    component sub_if_geq
        generic (THRESHOLD : unsigned);
        port ( x_in : in std_logic_vector (7 downto 0);
               x_out : out std_logic_vector (7 downto 0)
              );
    end component;
    
    signal A_unsigned_congruent : std_logic_vector (7 downto 0);
    signal convert_signed : std_logic;
    signal connect_192_96, connect_96_48, connect_48_24, connect_24_12, connect_12_6, connect_6_3 : std_logic_vector(7 downto 0);
begin
    --No conversion needed for positive signed numbers
    --Negative signed numbers are congruent with their unsigned interpretation -1
    convert_signed <= A_in(7) and signed_mode;
    A_unsigned_congruent <= 
        std_logic_vector(unsigned(A_in) - 1) when (convert_signed = '1') else
        A_in;
    
        sub_192 : sub_if_geq
                generic map (THRESHOLD => to_unsigned(192, 8))
                port map (x_in => A_unsigned_congruent, x_out => connect_192_96);
        sub_96 : sub_if_geq
                generic map (THRESHOLD => to_unsigned(96, 8))
                port map (x_in => connect_192_96, x_out => connect_96_48);
        sub_48 : sub_if_geq
                generic map (THRESHOLD => to_unsigned(48, 8))
                port map (x_in => connect_96_48, x_out => connect_48_24);
        sub_24 : sub_if_geq
                generic map (THRESHOLD => to_unsigned(24, 8))
                port map (x_in => connect_48_24, x_out => connect_24_12);
        sub_12 : sub_if_geq
                generic map (THRESHOLD => to_unsigned(12, 8))
                port map (x_in => connect_24_12, x_out => connect_12_6);
        sub_6 : sub_if_geq
                generic map (THRESHOLD => to_unsigned(6, 8))
                port map (x_in => connect_12_6, x_out => connect_6_3);
        sub_3 : sub_if_geq
                generic map (THRESHOLD => to_unsigned(3, 8))
                port map (x_in => connect_6_3, x_out => A_mod);
end behavioral;