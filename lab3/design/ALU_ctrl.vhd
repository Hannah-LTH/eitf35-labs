library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ALU_ctrl is
   port ( clk     : in  std_logic;
          reset   : in  std_logic;
          enter   : in  std_logic;
          sign    : in  std_logic;
          FN      : out std_logic_vector (3 downto 0);   -- ALU functions
          RegCtrl : out std_logic_vector (1 downto 0)   -- Register update control bits
        );
end ALU_ctrl;

architecture behavioral of ALU_ctrl is

-- SIGNAL DEFINITIONS HERE IF NEEDED
-- SIGNAL DEFINITIONS HERE IF NEEDED
type state_type is (s_0, s_1, s_2, s_3, s_4, s_5, s_6, s_7);
signal current_state, next_state: state_type;
begin

--sequential for states machine
process(clk,reset)
begin
    if rising_edge(clk) then
        if reset='1' then
           current_state<=s_0;
        else current_state<=next_state;
        end if;
    end if;
end process;

combinational: process (enter, sign, current_state)
    begin
    --set default
           next_state <= current_state;
           FN <= (others=>'0');
           RegCtrl<="00";
           
       case current_state is
           when s_0=>
             FN<="0000";
             RegCtrl<="01";
             if enter='1' then
                next_state<=s_1;
             else
                next_state<=s_0;
            end if;
            
           when s_1=>
            FN<="0001";
            RegCtrl<="10";
            if enter='1' then
               next_state<=s_2;
            else 
               next_state<=s_1;
            end if;
            
            when s_2=>
            FN<="0010";
            if enter='1' then
               next_state<=s_3;
            elsif sign='1' then
               next_state<=s_5;
            else
               next_state<=s_2;
            end if;
            
            when s_3=>
            FN<="0011";
            if enter='1' then
               next_state<=s_4;
            elsif sign='1' then
               next_state<=s_6;
            else
               next_state<=s_3;
            end if;
            
            when s_4=>
            FN<="0100";
            if enter='1' then
               next_state<=s_2;
            elsif sign='1' then
               next_state<=s_7;
            else
               next_state<=s_4;
            end if;
           
            when s_5=>
            FN<="1010";
            if enter='1' then
               next_state<=s_6;
            elsif sign='1' then
               next_state<=s_2;
            else
               next_state<=s_5;
            end if;
         
            when s_6=>
            FN<="1011";
            if enter='1' then
               next_state<=s_7;
            elsif sign='1' then
               next_state<=s_3;
            else
               next_state<=s_6;
            end if;
            
            when s_7=>
            FN<="1100";
            if enter='1' then
               next_state<=s_5;
            elsif sign='1' then
               next_state<=s_4;
            else
               next_state<=s_7;
            end if;
          end case;
end process;           

end behavioral;




