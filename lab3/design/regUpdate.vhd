library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity regUpdate is
   port ( clk        : in  std_logic;
          reset      : in  std_logic;
          RegCtrl    : in  std_logic_vector (1 downto 0);   -- Register update control from ALU controller
          input      : in  std_logic_vector (7 downto 0);   -- Switch inputs
          A          : out std_logic_vector (7 downto 0);   -- Input A
          B          : out std_logic_vector (7 downto 0)    -- Input B
        );
end regUpdate;

architecture behavioral of regUpdate is

    signal A_reg, A_next, B_reg, B_next : std_logic_vector(7 downto 0);

begin

    process(clk, reset)
    begin
        if (reset = '1') then
            A_reg <= (others => '0');
            B_reg <= (others => '0');
        elsif rising_edge(clk) then
            A_reg <= A_next;
            B_reg <= B_next;
        end if;
    end process;
    
    A_next <= input when RegCtrl = "01" else
              A_reg;
    B_next <= input when RegCtrl = "10" else
              B_reg;
    A <= A_reg;
    B <= B_reg;

end behavioral;
