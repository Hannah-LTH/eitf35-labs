library ieee;
use ieee.std_logic_1164.all;

library work;
use work.ALU_components_pack.all;

entity ALU_top is
   port ( clk        : in  std_logic;
          b_reset      : in  std_logic;
          b_Enter    : in  std_logic;
          b_Sign     : in  std_logic;
          input      : in  std_logic_vector(7 downto 0);
          seven_seg  : out std_logic_vector(6 downto 0);
          anode      : out std_logic_vector(3 downto 0)
        );
end ALU_top;

architecture structural of ALU_top is

  component ALU_ctrl is
   port ( clk     : in  std_logic;
          reset   : in  std_logic;
          enter   : in  std_logic;
          sign    : in  std_logic;
          FN      : out std_logic_vector (3 downto 0);   -- ALU functions
          RegCtrl : out std_logic_vector (1 downto 0)   -- Register update control bits
        );
    end component;
    
    component ALU is
       port ( A          : in  std_logic_vector (7 downto 0);   -- Input A
              B          : in  std_logic_vector (7 downto 0);   -- Input B
              FN         : in  std_logic_vector (3 downto 0);   -- ALU functions provided by the ALU_Controller (see the lab manual)
              result     : out std_logic_vector (7 downto 0);   -- ALU output (unsigned binary)
              overflow   : out std_logic;                       -- '1' if overflow ocurres, '0' otherwise 
              sign       : out std_logic                        -- '1' if the result is a negative value, '0' otherwise
            );
    end component;
    
    component regUpdate is
       port ( clk        : in  std_logic;
              reset      : in  std_logic;
              RegCtrl    : in  std_logic_vector (1 downto 0);   -- Register update control from ALU controller
              input      : in  std_logic_vector (7 downto 0);   -- Switch inputs
              A          : out std_logic_vector (7 downto 0);   -- Input A
              B          : out std_logic_vector (7 downto 0)    -- Input B
            );
    end component;
    
    component binary2BCD is
       generic ( WIDTH : integer := 8   -- 8 bit binary to BCD
               );
       port ( binary_in : in  std_logic_vector(WIDTH-1 downto 0);  -- binary input width
              BCD_out   : out std_logic_vector(9 downto 0)        -- BCD output, 10 bits [2|4|4] to display a 3 digit BCD value when input has length 8
            );
    end component;
    
    component seven_seg_driver is
       port ( clk           : in  std_logic;
              reset         : in  std_logic;
              BCD_digit     : in  std_logic_vector(9 downto 0);          
              sign          : in  std_logic;
              overflow      : in  std_logic;
              DIGIT_ANODE   : out std_logic_vector(3 downto 0);
              SEGMENT       : out std_logic_vector(6 downto 0)
            );
    end component;    
    
    component edge_detector is
        port (
             clk : in std_logic;
             reset : in std_logic;
             button : in std_logic;
             edge_found : out std_logic
         );
    end component;

   -- SIGNAL DEFINITIONS
   signal reset : std_logic;
   signal Enter, Enter_rising, Sign, Sign_rising : std_logic;
   signal ALU_FN : std_logic_vector (3 downto 0);
   signal reg_ctrl : std_logic_vector (1 downto 0);
   signal ALU_A, ALU_B, ALU_result : std_logic_vector(7 downto 0);
   signal ALU_overflow, ALU_sign : std_logic;
   signal BCD : std_logic_vector(9 downto 0);

begin
    reset <= not b_reset;
   -- to provide a clean signal out of a bouncy one coming from the push button
   -- input(b_Enter) comes from the pushbutton; output(Enter) goes to the FSM 
    debouncer1: debouncer
    port map ( clk          => clk,
              reset        => reset,
              button_in    => b_Enter,
              button_out   => Enter
            );
    debouncer2: debouncer
    port map (  clk => clk, 
                reset => reset,
                button_in => b_Sign,
                button_out => Sign
              );
    
    edge_detector1 : edge_detector
    port map (  clk => clk,
                reset => reset,
                button => Enter,
                edge_found => Enter_rising
              );
    edge_detector2 : edge_detector
    port map (  clk => clk,
                reset => reset,
                button => Sign,
                edge_found => Sign_rising
              );                 
    
    ALU_control : ALU_ctrl
    port map (  clk => clk,
                reset => reset,
                enter => Enter_rising,
                sign => Sign_rising,
                FN => ALU_FN,
                RegCtrl => reg_ctrl
              );
    
    ALU_unit : ALU
    port map (  A => ALU_A,
                B => ALU_B,
                FN => ALU_FN,
                result => ALU_result,
                overflow => ALU_overflow,
                sign => ALU_sign
              );
              
    input_reg : regUpdate
    port map (  clk => clk,
                reset => reset,
                RegCtrl => reg_ctrl,
                input => input,
                A => ALU_A,
                B => ALU_B
              );
    
    bcd_converter : binary2BCD
    port map (  binary_in => ALU_result,
                BCD_out => BCD
              );
              
    seven_sg_driver : seven_seg_driver
    port map (  clk => clk,
                reset => reset,
                BCD_digit => BCD,
                sign => ALU_sign,
                overflow => ALU_overflow,
                DIGIT_ANODE => anode,
                SEGMENT => seven_seg
              );
end structural;
