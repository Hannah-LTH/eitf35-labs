library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;


entity binary_to_sg is
    port (
	     binary_in : in std_logic_vector(3 downto 0);
	     sev_seg   : out std_logic_vector(6 downto 0)
	 );
end binary_to_sg;

architecture binary_to_sg_arch of binary_to_sg is
begin
    with binary_in select
        sev_seg <= (6 => '1',       others => '0')  when "0000",
                   (1|2 => '0',     others => '1')  when "0001",
                   (2|5 => '1',     others => '0')  when "0010",
                   (4|5 => '1',     others => '0')  when "0011",
                   (1|2|5|6 => '0', others => '1')  when "0100",
                   (1|4 => '1',     others => '0')  when "0101",
                   (1 => '1',       others => '0')  when "0110",
                   (0|1|2 => '0',   others => '1')  when "0111",
                   (                others => '0')  when "1000",
                   (3|4 => '1',     others => '0')  when "1001",
                   (1|2|3 => '1',   others => '0')  when "1010", --Overflow (F)
                   (6 => '0',       others => '1')  when "1011", --Minus sign
                   (                others => '1')  when "1100", --Empty
                   (1|2 => '1',     others => '0')  when others;
end binary_to_sg_arch;