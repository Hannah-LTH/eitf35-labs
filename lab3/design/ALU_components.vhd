library ieee;
use ieee.std_logic_1164.all;

package ALU_components_pack is

   -- Button debouncing 
   component debouncer   
   port ( clk        : in  std_logic;
          reset      : in  std_logic;
          button_in  : in  std_logic;
          button_out : out std_logic
        );
   end component;
   
   -- D-flipflop
   component dff
   generic ( W : integer );
   port ( clk     : in  std_logic;
          reset   : in  std_logic;
          d       : in  std_logic_vector(W-1 downto 0);
          q       : out std_logic_vector(W-1 downto 0)
        );
   end component;
   
   -- ADD MORE COMPONENTS HERE IF NEEDED 

 -- shift and add3(binary2BCD algorithm)
   component binary2BCD_algorithm
   port(  input : in std_logic_vector(17 downto 0);
          output: out std_logic_vector(17 downto 0)
         );
   end component;
   
   
end ALU_components_pack;

-------------------------------------------------------------------------------
-- ALU component pack body
-------------------------------------------------------------------------------
package body ALU_components_pack is

end ALU_components_pack;

-------------------------------------------------------------------------------
-- debouncer component: There is no need to use this component, thogh if you get 
--                      unwanted moving between states of the FSM because of pressing
--                      push-button this component might be useful.
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity debouncer is
   port ( clk        : in  std_logic;
          reset      : in  std_logic;
          button_in  : in  std_logic;
          button_out : out std_logic
        );
end debouncer;

architecture behavioral of debouncer is

   signal count      : unsigned(19 downto 0);  -- Range to count 20ms with 50 MHz clock
   signal button_tmp : std_logic;
   
begin

process ( clk )
begin
   if clk'event and clk = '1' then
      if reset = '1' then
         count <= (others => '0');
      else
         count <= count + 1;
         button_tmp <= button_in;
         
         if (count = 0) then
            button_out <= button_tmp;
         end if;
      end if;
  end if;
end process;

end behavioral;

------------------------------------------------------------------------------
-- component dff - D-FlipFlop 
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity dff is
   generic ( W : integer
           );
   port ( clk     : in  std_logic;
          reset   : in  std_logic;
          d       : in  std_logic_vector(W-1 downto 0);
          q       : out std_logic_vector(W-1 downto 0)
        );
end dff;

architecture behavioral of dff is

begin

   process ( clk )
   begin
      if clk'event and clk = '1' then
         if reset = '1' then
            q <= (others => '0');
         else
            q <= d;
         end if;
      end if;
   end process;              

end behavioral;

------------------------------------------------------------------------------
-- BEHAVORIAL OF THE ADDED COMPONENETS HERE
-------------------------------------------------------------------------------
--component for binary to BCD
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity binary2BCD_algorithm is
  port(  input : in std_logic_vector(17 downto 0);
          output: out std_logic_vector(17 downto 0)
         );
end binary2BCD_algorithm;

architecture behavioral of binary2BCD_algorithm is
signal temp, result_temp: std_logic_vector(17 downto 0);
signal one_temp_result,  ten_temp_result: std_logic_vector(3 downto 0);
signal one_temp,ten_temp: std_logic_vector(3 downto 0);
signal hundred_temp: std_logic_vector(1 downto 0); 

begin

   process ( input,temp,one_temp,ten_temp, hundred_temp,one_temp_result,ten_temp_result)
   begin
            temp<=input;
            one_temp<=temp(11 downto 8);
            ten_temp<=temp(15 downto 12);
            hundred_temp<=temp(17 downto 16);

            if one_temp>"0100" then
                one_temp_result<=one_temp+3;
            else
                one_temp_result<=one_temp;
            end if;

            if ten_temp>"0100" then
                ten_temp_result<=ten_temp+3;
            else
                ten_temp_result<=ten_temp;
            end if;
            
            output<=hundred_temp&ten_temp_result&one_temp_result&temp(7 downto 0);
            temp<=input(16 downto 0)&'0';
    end process;   
    
    end behavioral;  


    
