library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ALU is
   port ( A          : in  std_logic_vector (7 downto 0);   -- Input A
          B          : in  std_logic_vector (7 downto 0);   -- Input B
          FN         : in  std_logic_vector (3 downto 0);   -- ALU functions provided by the ALU_Controller (see the lab manual)
          result 	   : out std_logic_vector (7 downto 0);   -- ALU output (unsigned binary)
	       overflow   : out std_logic;                       -- '1' if overflow ocurres, '0' otherwise 
	       sign       : out std_logic                        -- '1' if the result is a negative value, '0' otherwise
        );
end ALU;

architecture behavioral of ALU is
    component mod_3_unit is
        port ( A_in : in std_logic_vector (7 downto 0);
               signed_mode : in std_logic;
               A_mod : out std_logic_vector (7 downto 0)
              );
     end component;

    constant pass_A : std_logic_vector(3 downto 0) := "0000";
    constant pass_B : std_logic_vector(3 downto 0) := "0001";
    constant add_unsigned : std_logic_vector(3 downto 0) := "0010";
    constant sub_unsigned : std_logic_vector(3 downto 0) := "0011";
    constant mod_unsigned : std_logic_vector(3 downto 0) := "0100";
    constant add_signed : std_logic_vector(3 downto 0) := "1010";
    constant sub_signed : std_logic_vector(3 downto 0) := "1011";
    constant mod_signed : std_logic_vector(3 downto 0) := "1100";
    
    signal B_prime, sum, mod_out : std_logic_vector(7 downto 0);
    signal sub_mode : unsigned(0 downto 0);
    signal signed_mod : std_logic;
    signal neg_sum, trunc_pad_sum : std_logic_vector(7 downto 0);
    signal sign_s : std_logic;
        
begin
    
    sign <= sign_s;

    mod_3_instance : mod_3_unit
        port map (A_in => A, signed_mode => signed_mod, A_mod => mod_out);
        
    signed_mod <= '1' when FN = mod_signed else
                  '0';
    
    sum <= std_logic_vector(unsigned(A) + unsigned(B_prime) + sub_mode);
    trunc_pad_sum <= '0' & sum(6 downto 0);
    neg_sum <= std_logic_vector(to_unsigned(128, 8) - unsigned(trunc_pad_sum)) when sign_s = '1' else
               sum;
    
    with FN select B_prime <=
        not B when sub_unsigned | sub_signed,
        B when others;
        
    with FN select sub_mode <=
        to_unsigned(1, 1) when sub_unsigned | sub_signed,
        to_unsigned(0, 1) when others;
        
    with FN select result <= 
        A when pass_A,
        B when pass_B,
        sum when add_unsigned | sub_unsigned,
        neg_sum when  add_signed | sub_signed,
        mod_out when mod_unsigned | mod_signed,
        "10101010" when others;
        
    with FN select sign_s <=
        sum(7) when add_signed | sub_signed,
        '0' when others;
        
    with FN select overflow <=
        (A(7) and B_prime(7) and (not sum(7))) or ((not A(7)) and (not B_prime(7)) and sum(7)) when add_signed | sub_signed,
        '0' when others;
end behavioral;
