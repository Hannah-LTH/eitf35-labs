library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity tb_binary2BCD is
end tb_binary2BCD;

architecture Behavioral of tb_binary2BCD is
    component binary2BCD
    port(
          binary_in : in  std_logic_vector(7 downto 0); 
          BCD_out   : out std_logic_vector(9 downto 0) 
    );
    end component;
    
signal    binary_in :  std_logic_vector(7 downto 0); 
signal    BCD_out   :  std_logic_vector(9 downto 0); 
begin
    binary_in<="00000000"after 0ns,
               "10000000"after 200ns,
               "01000000"after 400ns,
               "00100000"after 600ns,
               "10000000"after 800ns,
               "01000000"after 1000ns,
               "00100000"after 1200ns;
    
    binary21BCD: binary2BCD
    port map(
     binary_in => binary_in, 
      BCD_out=> BCD_out
      );
end Behavioral;
