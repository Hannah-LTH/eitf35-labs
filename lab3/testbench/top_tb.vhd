library ieee;
use ieee.std_logic_1164.all;

entity top_tb is
end top_tb;

architecture structural of top_tb is

   component ALU_top
   port ( clk        : in  std_logic;
          b_reset    : in  std_logic;
          b_Enter    : in  std_logic;
          b_Sign     : in  std_logic;
          input      : in  std_logic_vector(7 downto 0);
          seven_seg  : out std_logic_vector(6 downto 0);
          anode      : out std_logic_vector(3 downto 0)
         );
   end component;
   
   signal clk : std_logic := '0';
   signal b_reset, b_Enter, b_Sign : std_logic;
   signal input : std_logic_vector(7 downto 0);
   signal seven_seg : std_logic_vector(6 downto 0);
   signal anode : std_logic_vector(3 downto 0);
   
   constant period   : time := 21ms;

begin  -- structural
   
   DUT: ALU_top
   port map (   clk => clk,
                b_reset => b_reset,
                b_Enter => b_Enter,
                b_Sign => b_Sign,
                input => input,
                seven_seg => seven_seg,
                anode => anode
            );
   
   -- *************************
   -- User test data pattern
   -- *************************
   clk <= not(clk) after 10 ns;
   b_reset <= '0',
              '1' after 10 ns;

   
   input <= "00000101",                    -- A = 5
          "00001001" after 1 * period;  -- A = 9
--        "00010001" after 2 * period,   -- A = 17
--        "10010001" after 3 * period,   -- A = 145
--        "10010100" after 4 * period,   -- A = 148
--        "11010101" after 5 * period,   -- A = 213
--        "00100011" after 6 * period,   -- A = 35
--        "11110010" after 7 * period,   -- A = -14
--        "00110001" after 8 * period,   -- A = 49
--        "01010101" after 9 * period,   -- A = 85
--        "01111111" after 10 * period,  -- A = 127
--        "10000000" after 11 * period;  -- A = -128
   
   b_Enter <= '0',
              '1' after 1 * period,
              '0' after 2 * period,
              '1' after 3 * period,
              '0' after 4 * period,
              '1' after 5 * period,
              '0' after 6 * period,
              '1' after 7 * period,
              '0' after 8 * period;
    b_Sign <= '0',
              '1' after 4 * period,
              '0' after 5 * period,
              '1' after 6 * period,
              '0' after 7 * period,
              '1' after 8 * period, 
              '0' after 9 * period;
            
end structural;
