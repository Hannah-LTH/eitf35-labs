library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;


entity unit_max_pooling is
    generic(row: unsigned;
            column: unsigned
            );
    Port ( clk : in std_logic;
           reset : in std_logic;
           row_count: in std_logic_vector (5 downto 0);
           column_count: in std_logic_vector (5 downto 0);
           data_in : in std_logic_vector(3 downto 0);
           result: out std_logic_vector(3 downto 0)
           );
end unit_max_pooling;


architecture Behavioral of unit_max_pooling is
--to compare and store the max value for 15 data.
signal row_max_next,row_max_current: std_logic_vector(3 downto 0);
--to compare and store the max value for 15*15 data.
signal data_temp: std_logic_vector(3 downto 0);

begin
    
--seqencial for the registers which store values
process(clk,reset)
begin
    if reset='1' then
            row_max_current<=(others=>'0');

        else if rising_edge(clk) then
            row_max_current<=row_max_next;

        end if;
    end if;
end process;

--combinational to compare data and store the bigger one.
    data_temp<=data_in when data_in>row_max_current  else 
               row_max_current;
--15*15 matrix
    row_max_next<=data_temp when row_count>=std_logic_vector(row)and row_count<std_logic_vector(row+15) and column_count>=std_logic_vector(column) and column_count<std_logic_vector(column+15) else
                  row_max_current;
--look ahead
    result<=row_max_next;
  
end Behavioral;