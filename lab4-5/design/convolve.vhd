library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity convolve is
    Port ( clk : in std_logic;
           reset : in std_logic;
           result_to_mp: out std_logic_vector(3 downto 0);
           result_out_valid: out std_logic
           );
end convolve;

architecture Behavioral of convolve is
component unit_convolve is
    generic (column_sel: std_logic_vector(2 downto 0);
             filter_sel: std_logic_vector(1 downto 0));
    Port ( clk : in std_logic;
           reset : in std_logic;
           data_to_ram: in std_logic_vector(63 downto 0);
           result_valid: in std_logic;
           data_out_ram_change: in std_logic;
           data_out_ram_select: in std_logic;
           column_sel_count : in std_logic_vector(2 downto 0);
           filter_sel_count : in std_logic_vector(1 downto 0);
           result : out signed(7 downto 0));
end component;
component dist_mem_gen_1
  port (
    a : in std_logic_vector(5 downto 0);
    d : in std_logic_vector(63 downto 0);
    clk : in std_logic;
    we : in std_logic;
    spo : out std_logic_vector(63 downto 0)
  );
end component;
signal result_valid: std_logic;
signal data_to_ram: std_logic_vector(63 downto 0);
signal column_sel_count, column_sel_count_next:  std_logic_vector(2 downto 0);
signal filter_sel_count, filter_sel_count_next:  std_logic_vector(1 downto 0);
--to count 64 time
signal counter_64_delay_1,counter_64_delay,counter_64,counter_64_next: std_logic_vector(5 downto 0);
signal counter_60,counter_60_next: std_logic_vector(5 downto 0);
signal sum_0, sum_1, sum_2, sum_3, result: signed(7 downto 0);
signal data_out_ram_change, data_out_ram_select: std_logic;
begin
    process(clk,reset)
    begin
         if reset='1' then
                column_sel_count<=(others=>'1');
                filter_sel_count<=(others=>'0');
                counter_64<=(others=>'0');
                counter_64_delay<=(others=>'0');
                counter_64_delay_1<=(others=>'0');
                counter_60<=(others=>'0');
            else if rising_edge(clk) then
                column_sel_count<=column_sel_count_next;
                filter_sel_count<=filter_sel_count_next;
                counter_64<=counter_64_next;
                counter_64_delay<=counter_64;
                counter_64_delay_1<=counter_64_delay;
                counter_60<=counter_60_next;
            end if;
        end if;
    end process;
    --change
    column_sel_count_next<=(others=>'0') when column_sel_count="011" else
                            column_sel_count+1;
    --
    filter_sel_count_next<=filter_sel_count+1 when result_valid='1'and counter_60="111011" else
                           filter_sel_count;
                         
    --results become valid after 64 clock cycles
    counter_64_next<="111111" when counter_64="111111" else--63
                     counter_64 +1;
    result_valid<='1' when counter_64_delay_1="111111" else --data_valid
                 '0';
    --mod60 counter
    counter_60_next<=(others=>'0') when counter_60="111011"  else--59
                   counter_60 when result_valid='0' else
                   counter_60+1;
    
    data_out_ram_change<='1' when counter_60="000000" and result_valid='1' else
                         '0';
    data_out_ram_select<='1'when counter_60="111011" and result_valid='1' else
                         '0';
    
   --ram that stores the coe file              
    ram64_64 : dist_mem_gen_1
    port map ( a => counter_64,
               d => (others=>'0'),
               clk => clk,
               we => '0',
               spo => data_to_ram);
               
   conv1: unit_convolve
             generic map (column_sel=>"000",
                          filter_sel=>"00")
             port map ( clk=>clk, reset=>reset, 
                        data_out_ram_change=>data_out_ram_change, 
                        data_out_ram_select=>data_out_ram_select,
                        data_to_ram=>data_to_ram, 
                        result_valid=>result_valid, 
                        column_sel_count=>column_sel_count, 
                        filter_sel_count=>filter_sel_count, 
                        result=>sum_0);
   conv2: unit_convolve
             generic map (column_sel=>"001",
                          filter_sel=>"01")
             port map ( clk=>clk, 
                        reset=>reset, 
                        data_out_ram_change=>data_out_ram_change, 
                        data_out_ram_select=>data_out_ram_select,
                        data_to_ram=>data_to_ram, 
                        result_valid=>result_valid, 
                        column_sel_count=>column_sel_count, 
                        filter_sel_count=>filter_sel_count, 
                        result=>sum_1);
   conv3: unit_convolve
             generic map (column_sel=>"010",
                          filter_sel=>"10")
             port map ( clk=>clk, 
                        reset=>reset, 
                        data_out_ram_change=>data_out_ram_change,
                        data_out_ram_select=>data_out_ram_select, 
                        data_to_ram=>data_to_ram, 
                        result_valid=>result_valid, 
                        column_sel_count=>column_sel_count, 
                        filter_sel_count=>filter_sel_count, 
                        result=>sum_2);
   conv4: unit_convolve
             generic map (column_sel=>"011",
                          filter_sel=>"11")
             port map ( clk=>clk, 
                        reset=>reset, 
                        data_out_ram_change=>data_out_ram_change, 
                        data_out_ram_select=>data_out_ram_select,
                        data_to_ram=>data_to_ram, 
                        result_valid=>result_valid, 
                        column_sel_count=>column_sel_count, 
                        filter_sel_count=>filter_sel_count, 
                        result=>sum_3);
  
   result<=sum_0+sum_1+sum_2+sum_3;
  --relu, result to max pooling
   result_out_valid<=result_valid;
   result_to_mp<=std_logic_vector(result(3 downto 0)) when result>0 else
                 "0000";  
end Behavioral;
