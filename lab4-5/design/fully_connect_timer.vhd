library ieee;
library work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.util_pkg.all;

entity fully_connect_timer is
    generic(
        COUNT: natural
    );
    port(
        clk, reset: in std_logic;
        start: in std_logic;
        output: out std_logic
    );
end fully_connect_timer;

architecture behavioral of fully_connect_timer is
    signal t_reg, t_next: std_logic;
    signal count_reg, count_next: unsigned(log2c(COUNT + 1) - 1 downto 0);
    signal t_enable, count_eq: std_logic;
begin
    process(clk, reset)
    begin
        if reset = '1' then
            t_reg <= '0';
            count_reg <= (others => '0');
        elsif rising_edge(clk) then
            t_reg <= t_next;
            count_reg <= count_next;
        end if;
    end process;
    t_enable <= start or count_eq;
    t_next <= (not t_reg) when (t_enable = '1') else
               t_reg;
    count_next <= count_reg when (t_reg = '0') else 
                  (others => '0') when (count_eq = '1') else
                   count_reg + 1; 
    count_eq <= '1' when (count_reg = to_unsigned(COUNT, log2c(COUNT + 1))) else
                '0';
    output <= count_eq;
end behavioral;