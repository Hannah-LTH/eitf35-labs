library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity max_pooling is
    Port ( clk : in std_logic;
           reset : in std_logic;
           data_in_valid : in std_logic;
           data_in: in std_logic_vector(3 downto 0);
           max_pooling_done: out std_logic;
           result_1 : out std_logic_vector(3 downto 0);
           result_2 : out std_logic_vector(3 downto 0);
           result_3 : out std_logic_vector(3 downto 0);
           result_4 : out std_logic_vector(3 downto 0);
           result_5 : out std_logic_vector(3 downto 0);
           result_6 : out std_logic_vector(3 downto 0);
           result_7 : out std_logic_vector(3 downto 0);
           result_8 : out std_logic_vector(3 downto 0);
           result_9 : out std_logic_vector(3 downto 0);
           result_10 : out std_logic_vector(3 downto 0);
           result_11 : out std_logic_vector(3 downto 0);
           result_12 : out std_logic_vector(3 downto 0);
           result_13 : out std_logic_vector(3 downto 0);
           result_14 : out std_logic_vector(3 downto 0);
           result_15 : out std_logic_vector(3 downto 0);          
           result_16 : out std_logic_vector(3 downto 0));
end max_pooling;

architecture Behavioral of max_pooling is
    component unit_max_pooling
        generic (row : unsigned;
                 column: unsigned);
         Port ( clk : in std_logic;
                reset : in std_logic;
                row_count: in std_logic_vector (5 downto 0);
                column_count: in std_logic_vector (5 downto 0);
                data_in : in std_logic_vector(3 downto 0);
                result: out std_logic_vector(3 downto 0)
                );
    end component;

    signal row_count,row_count_next: std_logic_vector(5 downto 0);
    signal column_count,column_count_next: std_logic_vector(5 downto 0);
    signal column_enable: std_logic;
begin
    process(clk,reset)
    begin
         if reset='1'then
                row_count<=(others=>'0');
                column_count<=(others=>'0');
            else if rising_edge(clk) then
                row_count<=row_count_next;
                column_count<=column_count_next;
            end if;
        end if;
    end process;
--mod 60 counter 
    row_count_next<=(others=>'0') when row_count=std_logic_vector(to_unsigned(59,6)) else
                    row_count+1 when data_in_valid='1'else
                    row_count;
-- next row after 60 clock cycles
    column_enable<= '1' when row_count=std_logic_vector(to_unsigned(59,6)) else
                    '0';
    column_count_next<= column_count+1 when column_enable='1' else
                        column_count;
--indicate that max pooling has done
    max_pooling_done<='1' when column_count=std_logic_vector(to_unsigned(59,6)) else
                      '0';
--results of 16 matrics
    result1: unit_max_pooling
             generic map (row=>to_unsigned(0,6),
                          column=>to_unsigned(0,6))
             port map (clk=>clk, 
                       reset=>reset, 
                       data_in=>data_in, 
                       row_count=>row_count, 
                       column_count=>column_count, 
                       result=>result_16); 
    result2: unit_max_pooling
             generic map (row=>to_unsigned(15,6),
                          column=>to_unsigned(0,6))
             port map (clk=>clk, 
                       reset=>reset, 
                       data_in=>data_in, 
                       row_count=>row_count, 
                       column_count=>column_count, 
                       result=>result_15); 
    result3: unit_max_pooling
             generic map (row=>to_unsigned(30,6),
                          column=>to_unsigned(0,6))
             port map (clk=>clk, 
                       reset=>reset, 
                       data_in=>data_in, 
                       row_count=>row_count, 
                       column_count=>column_count, 
                       result=>result_14);
    result4: unit_max_pooling
             generic map (row=>to_unsigned(45,6),
                          column=>to_unsigned(0,6))
             port map (clk=>clk, 
                       reset=>reset, 
                       data_in=>data_in, 
                       row_count=>row_count, 
                       column_count=>column_count, 
                       result=>result_13); 
    result5: unit_max_pooling
             generic map (row=>to_unsigned(0,6),
                          column=>to_unsigned(15,6))
             port map (clk=>clk, 
                       reset=>reset, 
                       data_in=>data_in, 
                       row_count=>row_count, 
                       column_count=>column_count, 
                       result=>result_12); 
    result6: unit_max_pooling
             generic map (row=>to_unsigned(15,6),
                          column=>to_unsigned(15,6))
             port map (clk=>clk, 
                       reset=>reset, 
                       data_in=>data_in, 
                       row_count=>row_count, 
                       column_count=>column_count, 
                       result=>result_11);
    result7: unit_max_pooling
             generic map (row=>to_unsigned(30,6),
                          column=>to_unsigned(15,6))
             port map (clk=>clk, 
                       reset=>reset, 
                       data_in=>data_in, 
                       row_count=>row_count, 
                       column_count=>column_count, 
                       result=>result_10); 
    result8: unit_max_pooling
             generic map (row=>to_unsigned(45,6),
                          column=>to_unsigned(15,6))
             port map (clk=>clk, 
                       reset=>reset, 
                       data_in=>data_in, 
                       row_count=>row_count, 
                       column_count=>column_count, 
                       result=>result_9); 
    result9: unit_max_pooling
             generic map (row=>to_unsigned(0,6),
                          column=>to_unsigned(30,6))
             port map (clk=>clk, 
                       reset=>reset, 
                       data_in=>data_in, 
                       row_count=>row_count, 
                       column_count=>column_count, 
                       result=>result_8);
    result10: unit_max_pooling
             generic map (row=>to_unsigned(15,6),
                          column=>to_unsigned(30,6))
             port map (clk=>clk, 
                       reset=>reset, 
                       data_in=>data_in, 
                       row_count=>row_count, 
                       column_count=>column_count, 
                       result=>result_7); 
    result11: unit_max_pooling
             generic map (row=>to_unsigned(30,6),
                          column=>to_unsigned(30,6))
             port map (clk=>clk, 
                       reset=>reset, 
                       data_in=>data_in, 
                       row_count=>row_count, 
                       column_count=>column_count, 
                       result=>result_6); 
    result12: unit_max_pooling
             generic map (row=>to_unsigned(45,6),
                          column=>to_unsigned(30,6))
             port map (clk=>clk, 
                       reset=>reset, 
                       data_in=>data_in, 
                       row_count=>row_count, 
                       column_count=>column_count, 
                       result=>result_5);
    result13: unit_max_pooling
             generic map (row=>to_unsigned(0,6),
                          column=>to_unsigned(45,6))
             port map (clk=>clk, 
                       reset=>reset, 
                       data_in=>data_in, 
                       row_count=>row_count, 
                       column_count=>column_count, 
                       result=>result_4); 
    result14: unit_max_pooling
             generic map (row=>to_unsigned(15,6),
                          column=>to_unsigned(45,6))
             port map (clk=>clk, 
                       reset=>reset, 
                       data_in=>data_in, 
                       row_count=>row_count, 
                       column_count=>column_count, 
                       result=>result_3); 
    result15: unit_max_pooling
             generic map (row=>to_unsigned(30,6),
                          column=>to_unsigned(45,6))
             port map (clk=>clk, 
                       reset=>reset, 
                       data_in=>data_in, 
                       row_count=>row_count, 
                       column_count=>column_count, 
                       result=>result_2); 
    result16: unit_max_pooling
             generic map (row=>to_unsigned(45,6),
                          column=>to_unsigned(45,6))
             port map (clk=>clk, 
                       reset=>reset, 
                       data_in=>data_in, 
                       row_count=>row_count, 
                       column_count=>column_count, 
                       result=>result_1);                       
end Behavioral;


