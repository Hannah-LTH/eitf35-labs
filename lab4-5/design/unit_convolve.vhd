library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity unit_convolve is
    generic (column_sel: std_logic_vector(2 downto 0);
             filter_sel: std_logic_vector(1 downto 0));
    Port ( clk : in std_logic;
           reset : in std_logic;
           data_to_ram: in std_logic_vector(63 downto 0);
           result_valid: in std_logic;
           data_out_ram_change: in std_logic;
           data_out_ram_select: in std_logic;
           --change
           column_sel_count : in std_logic_vector(2 downto 0);
           filter_sel_count : in std_logic_vector(1 downto 0);
           result : out signed(7 downto 0));
end unit_convolve;
architecture Behavioral of unit_convolve is
    component dist_mem_gen_0
      port (
        a : in std_logic_vector(3 downto 0);
        d : in std_logic_vector(63 downto 0);
        clk : in std_logic;
        we : in std_logic;
        spo : out std_logic_vector(63 downto 0)
      );
    end component;
    --Pipeline
    signal result_reg, result_next: signed(7 downto 0);
    
    signal shift_reg, shift_reg_next: std_logic_vector(63 downto 0);
    --address for ram input and ouput
    signal in_addr_reg, in_addr_reg_next: std_logic_vector(3 downto 0);
    signal out_addr_reg, out_addr_reg_next: std_logic_vector(3 downto 0);
--    signal addr_out_sel, addr_out_sel_next: std_logic_vector(1 downto 0);
    signal addr: std_logic_vector(3 downto 0);
    --data go out of ram to shift reg
    signal data_out_ram: std_logic_vector(63 downto 0);
    signal data_a, data_b, data_c, data_d: signed(3 downto 0);
    signal write: std_logic;
    signal filter_a, filter_b, filter_c,filter_d:  signed(3 downto 0);
    constant filter00 : signed(3 downto 0) := "1101";-- -3
    constant filter01 : signed(3 downto 0) := "1111";-- -1
    constant filter02 : signed(3 downto 0) := "0001";-- 1
    constant filter03 : signed(3 downto 0) := "1111";-- -1
    constant filter10 : signed(3 downto 0) := "0010";-- 2
    constant filter11 : signed(3 downto 0) := "1110";-- -2
    constant filter12 : signed(3 downto 0) := "0000";-- 0
    constant filter13 : signed(3 downto 0) := "0000";-- 0
    constant filter20 : signed(3 downto 0) := "0000";-- 0
    constant filter21 : signed(3 downto 0) := "0011";-- 3
    constant filter22 : signed(3 downto 0) := "0000";-- 0
    constant filter23 : signed(3 downto 0) := "1101";-- -3
    constant filter30 : signed(3 downto 0) := "1111";-- -1
    constant filter31 : signed(3 downto 0) := "1110";-- -2
    constant filter32 : signed(3 downto 0) := "1110";-- -2
    constant filter33 : signed(3 downto 0) := "0100";-- 4   
begin
    process(clk,reset)
    begin
        if reset='1' then
                in_addr_reg<=(others=>'0');
                out_addr_reg<=(others=>'0');
                shift_reg<=(others=>'0');
                result_reg <= (others => '0');
            else if rising_edge(clk) then
                in_addr_reg<=in_addr_reg_next;
                out_addr_reg<=out_addr_reg_next;
                shift_reg<=shift_reg_next;
                result_reg <= result_next;                
            end if;
        end if;
    end process;
    
    --for writing ram change
    in_addr_reg_next<=in_addr_reg+1 when column_sel=column_sel_count  else
                      in_addr_reg;
    write<='1' when result_valid='0'and column_sel=column_sel_count   else
           '0';
           
    --for reading ram, address adds one when data_out_ram changes 

    out_addr_reg_next<=out_addr_reg+1 when  data_out_ram_select='1'and result_valid='1' and filter_sel=filter_sel_count else
                      out_addr_reg;
    
    addr<=in_addr_reg when result_valid='0' else
          out_addr_reg;
         
    
    --select filter
    filter_a<= filter00 when filter_sel=filter_sel_count else
               filter10 when filter_sel=filter_sel_count+1 else
               filter20 when filter_sel=filter_sel_count+2 else
               filter30;
    filter_b<= filter01 when filter_sel=filter_sel_count else
               filter11 when filter_sel=filter_sel_count+1 else
               filter21 when filter_sel=filter_sel_count+2 else
               filter31;
    filter_c<= filter02 when filter_sel=filter_sel_count else
               filter12 when filter_sel=filter_sel_count+1 else
               filter22 when filter_sel=filter_sel_count+2 else
               filter32;
    filter_d<= filter03 when filter_sel=filter_sel_count else
               filter13 when filter_sel=filter_sel_count+1 else
               filter23 when filter_sel=filter_sel_count+2 else
               filter33;
    
    --shift the data to convolve 
    shift_reg_next<= data_out_ram(62 downto 0)&'0' when data_out_ram_change='1'else
                    shift_reg(62 downto 0)&'0';
                    
    --do convolution,look ahead
    data_a<="000"&shift_reg_next(63);
    data_b<="000"&shift_reg_next(62);
    data_c<="000"&shift_reg_next(61);
    data_d<="000"&shift_reg_next(60);          
    result_next<=(data_a*filter_a)+(data_b*filter_b)+(data_c*filter_c)+(data_d*filter_d);
    result <= result_reg;    
    
    --use 64*14 ram to store data    
        ram64_16 : dist_mem_gen_0
                  port map ( a => addr,
                             d => data_to_ram,
                             clk => clk,
                             we => write,
                             spo => data_out_ram);
end Behavioral;
