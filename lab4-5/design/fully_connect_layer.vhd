library ieee;
library work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.util_pkg.all;

entity fully_connect_layer is
    port(
        clk, reset: in std_logic;
        start: in std_logic;
        input: in std_logic_vector(63 downto 0);
        valid_output, is_square: out std_logic
    );
end fully_connect_layer;

architecture behavioral of fully_connect_layer is
    component fully_connect_node is
        generic(
            INPUT_COUNT: natural;
            INPUT_SIZE: natural;
            WEIGHT_SIZE: natural := 4;
            BIAS_SIZE: natural := 6
        );
        port(
            clk, reset: in std_logic;
            parallel_load: in std_logic;
            input: in std_logic_vector(INPUT_COUNT * INPUT_SIZE - 1 downto 0);
            weights: in std_logic_vector(INPUT_COUNT * WEIGHT_SIZE - 1 downto 0);
            bias: in signed(BIAS_SIZE - 1 downto 0);
            output: out std_logic_vector(sum_size(INPUT_SIZE, WEIGHT_SIZE, INPUT_COUNT) - 1 downto 0)
        );
    end component;
    
    component fully_connect_timer is
        generic(
            COUNT: natural
        );
        port(
            clk, reset: in std_logic;
            start: in std_logic;
            output: out std_logic
        );
    end component;
    
    signal out_1_1, out_1_2, out_1_3: std_logic_vector(13 downto 0);
    signal input_2: std_logic_vector(38 downto 0);
    signal out_2: std_logic_vector(19 downto 0);
    
    signal weights_1_1: std_logic_vector(63 downto 0) :=
        std_logic_vector(to_signed(2, 4)) & std_logic_vector(to_signed(2, 4)) & std_logic_vector(to_signed(4, 4)) & std_logic_vector(to_signed(3, 4)) & 
        std_logic_vector(to_signed(3, 4)) & std_logic_vector(to_signed(3, 4)) & std_logic_vector(to_signed(5, 4)) & std_logic_vector(to_signed(3, 4)) & 
        std_logic_vector(to_signed(5, 4)) & std_logic_vector(to_signed(6, 4)) & std_logic_vector(to_signed(4, 4)) & std_logic_vector(to_signed(3, 4)) & 
        std_logic_vector(to_signed(5, 4)) & std_logic_vector(to_signed(2, 4)) & std_logic_vector(to_signed(2, 4)) & std_logic_vector(to_signed(2, 4));
        
    signal weights_1_2: std_logic_vector(63 downto 0) :=
        std_logic_vector(to_signed( 3, 4)) & std_logic_vector(to_signed(-2, 4)) & std_logic_vector(to_signed(-2, 4)) & std_logic_vector(to_signed( 0, 4)) & 
        std_logic_vector(to_signed(-3, 4)) & std_logic_vector(to_signed(-1, 4)) & std_logic_vector(to_signed(-2, 4)) & std_logic_vector(to_signed( 0, 4)) & 
        std_logic_vector(to_signed(-5, 4)) & std_logic_vector(to_signed(-2, 4)) & std_logic_vector(to_signed( 1, 4)) & std_logic_vector(to_signed(-3, 4)) & 
        std_logic_vector(to_signed(-2, 4)) & std_logic_vector(to_signed( 2, 4)) & std_logic_vector(to_signed(-6, 4)) & std_logic_vector(to_signed( 2, 4));
    
    signal weights_1_3: std_logic_vector(63 downto 0) :=
        std_logic_vector(to_signed(-3, 4)) & std_logic_vector(to_signed( 1, 4)) & std_logic_vector(to_signed(-2, 4)) & std_logic_vector(to_signed( 0, 4)) & 
        std_logic_vector(to_signed(-3, 4)) & std_logic_vector(to_signed(-6, 4)) & std_logic_vector(to_signed(-2, 4)) & std_logic_vector(to_signed( 0, 4)) & 
        std_logic_vector(to_signed(-4, 4)) & std_logic_vector(to_signed( 1, 4)) & std_logic_vector(to_signed(-8, 4)) & std_logic_vector(to_signed( 3, 4)) & 
        std_logic_vector(to_signed(-3, 4)) & std_logic_vector(to_signed(-1, 4)) & std_logic_vector(to_signed( 3, 4)) & std_logic_vector(to_signed( 0, 4));
    
    signal weights_2: std_logic_vector(11 downto 0) :=
        std_logic_vector(to_signed( 7, 4)) & std_logic_vector(to_signed( 7, 4)) & std_logic_vector(to_signed(-8, 4));
        
    signal bias_1_1: signed(5 downto 0) := to_signed(-12, 6);
    signal bias_1_2: signed(5 downto 0) := to_signed(20, 6);
    signal bias_1_3: signed(5 downto 0) := to_signed(24, 6);
    signal bias_2: signed(5 downto 0) := to_signed(6, 6);
    
    signal start_2: std_logic;
    
begin
    
    input_2(12 downto 0) <= out_1_1(12 downto 0) when signed(out_1_1) >= 0 else
                            (others => '0');
    input_2(25 downto 13) <= out_1_2(12 downto 0) when signed(out_1_2) >= 0 else
                            (others => '0');
    input_2(38 downto 26) <= out_1_3(12 downto 0) when signed(out_1_3) >= 0 else
                            (others => '0');
    is_square <= '1' when signed(out_2) > 0 else
                 '0';

    node_1_1: fully_connect_node
        generic map(
            INPUT_COUNT => 16,
            INPUT_SIZE => 4
        )
        port map(
            clk => clk,
            reset => reset,
            parallel_load => start,
            input => input,
            weights => weights_1_1,
            bias => bias_1_1,
            output => out_1_1
        );
    
    node_1_2: fully_connect_node
        generic map(
            INPUT_COUNT => 16,
            INPUT_SIZE => 4
        )
        port map(
            clk => clk,
            reset => reset,
            parallel_load => start,
            input => input,
            weights => weights_1_2,
            bias => bias_1_2,
            output => out_1_2
        );

    node_1_3: fully_connect_node
        generic map(
            INPUT_COUNT => 16,
            INPUT_SIZE => 4
        )
        port map(
            clk => clk,
            reset => reset,
            parallel_load => start,
            input => input,
            weights => weights_1_3,
            bias => bias_1_3,
            output => out_1_3
        );
        
    node_2: fully_connect_node
        generic map(
            INPUT_COUNT => 3,
            INPUT_SIZE => 13
        )
        port map(
            clk => clk,
            reset => reset,
            parallel_load => start_2,
            input => input_2,
            weights => weights_2,
            bias => bias_2,
            output => out_2
        );
    
    timer_1: fully_connect_timer
        generic map(
            COUNT => 16
        )
        port map(
            clk => clk,
            reset => reset,
            start => start,
            output => start_2
        );
    
    timer_2: fully_connect_timer
        generic map(
            COUNT => 3
        )
        port map(
            clk => clk,
            reset => reset,
            start => start_2,
            output => valid_output
        );
end behavioral;