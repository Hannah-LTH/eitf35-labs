library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity cnn_top is
    port(
        clk, reset: in std_logic;
        valid_output, is_square: out std_logic
    );
end cnn_top;

architecture behavioral of cnn_top is
    component top is
        Port (
           clk  : in std_logic;
           reset   : in std_logic;
           max_pooling_done: out std_logic;
           result_1 : out std_logic_vector(3 downto 0);
           result_2 : out std_logic_vector(3 downto 0);
           result_3 : out std_logic_vector(3 downto 0);
           result_4 : out std_logic_vector(3 downto 0);
           result_5 : out std_logic_vector(3 downto 0);
           result_6 : out std_logic_vector(3 downto 0);
           result_7 : out std_logic_vector(3 downto 0);
           result_8 : out std_logic_vector(3 downto 0);
           result_9 : out std_logic_vector(3 downto 0);
           result_10 : out std_logic_vector(3 downto 0);
           result_11 : out std_logic_vector(3 downto 0);
           result_12 : out std_logic_vector(3 downto 0);
           result_13 : out std_logic_vector(3 downto 0);
           result_14 : out std_logic_vector(3 downto 0);
           result_15 : out std_logic_vector(3 downto 0);          
           result_16 : out std_logic_vector(3 downto 0) );
    end component;
    
    component fully_connect_layer is
        port(
            clk, reset: in std_logic;
            start: in std_logic;
            input: in std_logic_vector(63 downto 0);
            valid_output, is_square: out std_logic
        );
    end component;
    
    component edge_detector is
        port (
             clk : in std_logic;
             reset : in std_logic;
             button : in std_logic;
             edge_found : out std_logic
         );
    end component;
    
    signal max_pooling_done, start_fc: std_logic;
    signal fc_input: std_logic_vector(63 downto 0);
    
begin

    conv_and_pool: top
        port map(
            clk => clk,
            reset => reset,
            max_pooling_done => max_pooling_done,
            result_1 => fc_input(3 downto 0),
            result_2 => fc_input(7 downto 4),
            result_3 => fc_input(11 downto 8),
            result_4 => fc_input(15 downto 12),
            result_5 => fc_input(19 downto 16),
            result_6 => fc_input(23 downto 20),
            result_7 => fc_input(27 downto 24),
            result_8 => fc_input(31 downto 28),
            result_9 => fc_input(35 downto 32),
            result_10 => fc_input(39 downto 36),
            result_11 => fc_input(43 downto 40),
            result_12 => fc_input(47 downto 44),
            result_13 => fc_input(51 downto 48),
            result_14 => fc_input(55 downto 52),
            result_15 => fc_input(59 downto 56),
            result_16 => fc_input(63 downto 60)
        );
    
    fully_connect: fully_connect_layer
        port map(
            clk => clk,
            reset => reset,
            start => start_fc,
            input => fc_input,
            valid_output => valid_output,
            is_square => is_square
        );
    
    fc_start_detector: edge_detector
        port map(
            clk => clk,
            reset => reset,
            button => max_pooling_done,
            edge_found => start_fc
        );
end behavioral;