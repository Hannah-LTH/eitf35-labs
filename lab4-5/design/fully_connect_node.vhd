library ieee;
library work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.util_pkg.all;

entity fully_connect_node is
    generic(
        INPUT_COUNT: natural;
        INPUT_SIZE: natural;
        WEIGHT_SIZE: natural := 4;
        BIAS_SIZE: natural := 6
    );
    port(
        clk, reset: in std_logic;
        parallel_load: in std_logic;
        input: in std_logic_vector(INPUT_COUNT * INPUT_SIZE - 1 downto 0);
        weights: in std_logic_vector(INPUT_COUNT * WEIGHT_SIZE - 1 downto 0);
        bias: in signed(BIAS_SIZE - 1 downto 0);
        output: out std_logic_vector(sum_size(INPUT_SIZE, WEIGHT_SIZE, INPUT_COUNT) - 1 downto 0)
    );
end fully_connect_node;

architecture behavioral of fully_connect_node is

    --Product requires at most the sum of the operands' widths, input_factor is one wider than INPUT_SIZE
    constant PRODUCT_SIZE: natural := INPUT_SIZE + WEIGHT_SIZE + 1;
    constant SUM_SIZE: natural := sum_size(INPUT_SIZE, WEIGHT_SIZE, INPUT_COUNT);
    --The 0th value does not need a register
    constant INPUTS_REG_WIDTH: natural := (INPUT_COUNT - 1) * INPUT_SIZE;
    constant WEIGHTS_REG_WIDTH: natural := (INPUT_COUNT -1) * WEIGHT_SIZE;
    
    signal inputs_reg, inputs_next: std_logic_vector(INPUTS_REG_WIDTH - 1 downto 0);
    signal weights_reg, weights_next : std_logic_vector(WEIGHTS_REG_WIDTH - 1 downto 0);
    signal product_reg, product_next: signed(PRODUCT_SIZE - 1 downto 0);
    signal sum_reg, sum_next: signed(SUM_SIZE - 1 downto 0);
    
    --Pad the input with MSB = 0 for signed multiplication
    signal input_factor : signed(INPUT_SIZE downto 0);
    signal weight_factor : signed(WEIGHT_SIZE - 1 downto 0);
    
begin
    process(clk, reset)
    begin
        if reset = '1' then
            inputs_reg <= (others => '0');
            weights_reg <= (others => '0');
            product_reg <= (others => '0');
            sum_reg <= (others => '0');
        elsif rising_edge(clk) then
            inputs_reg <= inputs_next;
            weights_reg <= weights_next;
            product_reg <= product_next;
            sum_reg <= sum_next;
        end if;
    end process;
    
    process(inputs_reg, weights_reg, product_reg, parallel_load, input, weights, bias)
    begin
        if parallel_load = '1' then
            inputs_next <= input(input'high downto INPUT_SIZE);
            weights_next <= weights(weights'high downto WEIGHT_SIZE);
            input_factor <= signed('0' & input(INPUT_SIZE - 1 downto 0));
            weight_factor <= signed(weights(WEIGHT_SIZE - 1 downto 0));
            sum_next <= resize(signed(bias), sum_next'length);
        else
            --Pad with 0 for sign
            input_factor <= signed('0' & inputs_reg(INPUT_SIZE - 1 downto 0));
            weight_factor <= signed(weights_reg(WEIGHT_SIZE - 1 downto 0));
            sum_next <= sum_reg + resize(product_reg, sum_next'length);
            --Shift everything one input along
            inputs_next(inputs_next'high - INPUT_SIZE downto 0) <= inputs_reg(inputs_reg'high downto INPUT_SIZE);
            inputs_next(inputs_next'high downto inputs_next'high - INPUT_SIZE + 1) <= (others => '-');
            weights_next(weights_next'high - WEIGHT_SIZE downto 0) <= weights_reg(weights_reg'high downto WEIGHT_SIZE);
            weights_next(weights_next'high downto weights_next'high - WEIGHT_SIZE + 1) <= (others => '-');
        end if;
    end process;
    
    product_next <= input_factor * weight_factor;
    output <= std_logic_vector(sum_reg);

end behavioral;