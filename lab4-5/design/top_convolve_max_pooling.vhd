library IEEE;
use IEEE.STD_LOGIC_1164.ALL;



entity top is
  Port (   clk  : in std_logic;
           reset   : in std_logic;
           max_pooling_done: out std_logic;
           result_1 : out std_logic_vector(3 downto 0);
           result_2 : out std_logic_vector(3 downto 0);
           result_3 : out std_logic_vector(3 downto 0);
           result_4 : out std_logic_vector(3 downto 0);
           result_5 : out std_logic_vector(3 downto 0);
           result_6 : out std_logic_vector(3 downto 0);
           result_7 : out std_logic_vector(3 downto 0);
           result_8 : out std_logic_vector(3 downto 0);
           result_9 : out std_logic_vector(3 downto 0);
           result_10 : out std_logic_vector(3 downto 0);
           result_11 : out std_logic_vector(3 downto 0);
           result_12 : out std_logic_vector(3 downto 0);
           result_13 : out std_logic_vector(3 downto 0);
           result_14 : out std_logic_vector(3 downto 0);
           result_15 : out std_logic_vector(3 downto 0);          
           result_16 : out std_logic_vector(3 downto 0) );
end top;


architecture Behavioral of top is
component convolve
   port(
         clk   : in std_logic;
         reset   : in std_logic;
         result_to_mp: out std_logic_vector(3 downto 0);
         result_out_valid: out std_logic
        );
end component;
component max_pooling is
    Port ( clk : in std_logic;
           reset : in std_logic;
           data_in_valid : in std_logic;
           data_in: in std_logic_vector(3 downto 0);
           max_pooling_done: out std_logic;
           result_1 : out std_logic_vector(3 downto 0);
           result_2 : out std_logic_vector(3 downto 0);
           result_3 : out std_logic_vector(3 downto 0);
           result_4 : out std_logic_vector(3 downto 0);
           result_5 : out std_logic_vector(3 downto 0);
           result_6 : out std_logic_vector(3 downto 0);
           result_7 : out std_logic_vector(3 downto 0);
           result_8 : out std_logic_vector(3 downto 0);
           result_9 : out std_logic_vector(3 downto 0);
           result_10 : out std_logic_vector(3 downto 0);
           result_11 : out std_logic_vector(3 downto 0);
           result_12 : out std_logic_vector(3 downto 0);
           result_13 : out std_logic_vector(3 downto 0);
           result_14 : out std_logic_vector(3 downto 0);
           result_15 : out std_logic_vector(3 downto 0);          
           result_16 : out std_logic_vector(3 downto 0)
);
end component;
    
     
     signal result_to_mp_reg, result_to_mp_next    :  std_logic_vector(3 downto 0);
     signal result_out_valid_reg, result_out_valid_next :  std_logic;
begin
    
    process(clk, reset)
    begin
        if reset = '1' then
            result_to_mp_reg <= (others => '0');
            result_out_valid_reg <= '0';
        elsif rising_edge(clk) then
            result_to_mp_reg <= result_to_mp_next;
            result_out_valid_reg <= result_out_valid_next;
        end if;
    end process;

uut1: convolve
    port map(
            clk => clk ,
            reset => reset,  
            result_to_mp => result_to_mp_next,
            result_out_valid => result_out_valid_next
            );
    uut2: max_pooling
    port map(
            clk     =>   clk  ,
            reset   => reset,  
            data_in => result_to_mp_reg,
            data_in_valid => result_out_valid_reg,
            max_pooling_done=> max_pooling_done,
            result_1 => result_1,
            result_2 => result_2,
            result_3 => result_3,             
            result_4 => result_4,             
            result_5 => result_5,             
            result_6 => result_6,             
            result_7 => result_7,             
            result_8 => result_8,             
            result_9 => result_9,         
            result_10=> result_10,         
            result_11=> result_11,                
            result_12=> result_12,               
            result_13=> result_13,               
            result_14=> result_14,               
            result_15=> result_15,               
            result_16=> result_16                        
            );               
end Behavioral;