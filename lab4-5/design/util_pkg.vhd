library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package util_pkg is
    function log2c (n: integer) return integer;
    function sum_size (input_size: natural; weight_size: natural; input_count: natural) return natural;
end util_pkg;

package body util_pkg is
    function log2c (n: integer) return integer is
        variable m, p: integer;
    begin
        m := 0;
        p := 1;
        while p < n loop
            m := m + 1;
            p := p * 2;
        end loop;
        return m;
    end log2c;
    
    function sum_size (input_size: natural; weight_size: natural; input_count: natural) return natural is
        variable product_size: natural;
    begin
        product_size := INPUT_SIZE + WEIGHT_SIZE + 1;
        return product_size + log2c(input_count + 1);
    end sum_size;
end util_pkg;