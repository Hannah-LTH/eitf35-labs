library ieee;
library work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.util_pkg.all;

entity tb_fully_connect_timer is
end tb_fully_connect_timer;

architecture structural of tb_fully_connect_timer is
    component fully_connect_timer is
        generic(
            COUNT: natural
        );
        port(
            clk, reset: in std_logic;
            start: in std_logic;
            output: out std_logic
        );
    end component;
    
    signal clk: std_logic := '0';
    signal reset: std_logic;
    signal start, output, output_2: std_logic;
    
    constant period: time := 10 ns;
    
begin
    DUT_layer_1: fully_connect_timer
        generic map(
            COUNT => 16
        )
        port map(
            clk => clk,
            reset => reset,
            start => start,
            output => output
        );
    
    DUT_layer_2: fully_connect_timer
        generic map(
            COUNT => 3
        )
        port map(
            clk => clk,
            reset => reset,
            start => start,
            output => output_2
        );
    
    clk <= not clk after period/2;
    reset <= '1',
             '0' after period/2;
    start <= '0',
             '1' after period,
             '0' after 2*period;
end structural;