library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_cnn_top is
end tb_cnn_top;

architecture structural of tb_cnn_top is
    component cnn_top is
        port(
            clk, reset: in std_logic;
            valid_output, is_square: out std_logic
        );
    end component;
    
    signal clk: std_logic := '0';
    signal reset: std_logic;
    signal valid_output, is_square: std_logic;
    
    constant period: time := 10 ns;

begin
    DUT: cnn_top
        port map(
            clk => clk,
            reset => reset,
            valid_output => valid_output,
            is_square => is_square
        );

    clk <= not clk after period/2;
    reset <= '1',
             '0' after period;
end structural;