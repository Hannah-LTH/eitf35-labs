library ieee;
library work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.util_pkg.all;

entity tb_fully_connect_layer is
end tb_fully_connect_layer;

architecture structural of tb_fully_connect_layer is
    component fully_connect_layer is
        port(
            clk, reset: in std_logic;
            start: in std_logic;
            input: in std_logic_vector(63 downto 0);
            valid_output, is_square: out std_logic
        );
    end component;
    
    signal clk: std_logic := '0';
    signal reset: std_logic;
    signal start, valid_output, is_square: std_logic;
    signal input: std_logic_vector(63 downto 0);
    
    constant period: time := 10 ns;
    --Expected out_2 = 31
    constant square_input: std_logic_vector :=
        std_logic_vector(to_unsigned(0, 4)) & std_logic_vector(to_unsigned(0, 4)) & std_logic_vector(to_unsigned(0, 4)) & std_logic_vector(to_unsigned(0, 4)) & 
        std_logic_vector(to_unsigned(0, 4)) & std_logic_vector(to_unsigned(4, 4)) & std_logic_vector(to_unsigned(0, 4)) & std_logic_vector(to_unsigned(0, 4)) & 
        std_logic_vector(to_unsigned(0, 4)) & std_logic_vector(to_unsigned(1, 4)) & std_logic_vector(to_unsigned(1, 4)) & std_logic_vector(to_unsigned(0, 4)) & 
        std_logic_vector(to_unsigned(0, 4)) & std_logic_vector(to_unsigned(0, 4)) & std_logic_vector(to_unsigned(0, 4)) & std_logic_vector(to_unsigned(0, 4));
    
    --Expected out_2 = -426
    constant circle_input: std_logic_vector :=
        std_logic_vector(to_unsigned(0, 4)) & std_logic_vector(to_unsigned(0, 4)) & std_logic_vector(to_unsigned(0, 4)) & std_logic_vector(to_unsigned(0, 4)) & 
        std_logic_vector(to_unsigned(3, 4)) & std_logic_vector(to_unsigned(2, 4)) & std_logic_vector(to_unsigned(4, 4)) & std_logic_vector(to_unsigned(0, 4)) & 
        std_logic_vector(to_unsigned(1, 4)) & std_logic_vector(to_unsigned(0, 4)) & std_logic_vector(to_unsigned(3, 4)) & std_logic_vector(to_unsigned(0, 4)) & 
        std_logic_vector(to_unsigned(0, 4)) & std_logic_vector(to_unsigned(4, 4)) & std_logic_vector(to_unsigned(3, 4)) & std_logic_vector(to_unsigned(0, 4));
    
begin

    DUT: fully_connect_layer
        port map(
            clk => clk,
            reset => reset,
            start => start,
            input => input,
            valid_output => valid_output,
            is_square => is_square
        );

    clk <= not clk after period/2;
    reset <= '1',
             '0' after period/2;
    start <= '1' after period,
             '0' after period*2,
             '1' after period*30,
             '0' after period*31;
    input <= square_input,
             circle_input after period*3;

end structural;