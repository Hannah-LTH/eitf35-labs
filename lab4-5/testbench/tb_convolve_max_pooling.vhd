library IEEE;
use IEEE.STD_LOGIC_1164.ALL;



entity tb is
end tb;

architecture Behavioral of tb is
component top
   port(
         clk   : in std_logic;
         reset   : in std_logic;
         max_pooling_done: out std_logic;
           result_1 : out std_logic_vector(3 downto 0);
           result_2 : out std_logic_vector(3 downto 0);
           result_3 : out std_logic_vector(3 downto 0);
           result_4 : out std_logic_vector(3 downto 0);
           result_5 : out std_logic_vector(3 downto 0);
           result_6 : out std_logic_vector(3 downto 0);
           result_7 : out std_logic_vector(3 downto 0);
           result_8 : out std_logic_vector(3 downto 0);
           result_9 : out std_logic_vector(3 downto 0);
           result_10 : out std_logic_vector(3 downto 0);
           result_11 : out std_logic_vector(3 downto 0);
           result_12 : out std_logic_vector(3 downto 0);
           result_13 : out std_logic_vector(3 downto 0);
           result_14 : out std_logic_vector(3 downto 0);
           result_15 : out std_logic_vector(3 downto 0);          
           result_16 : out std_logic_vector(3 downto 0)

        );
end component;

     constant period1    : time := 10ns;
     signal clk          : std_logic := '0';
     signal reset        : std_logic;
     signal  max_pooling_done: std_logic;
     signal result_1, result_2,result_3,result_4,result_5,result_6,result_7,result_8,result_9,result_10,result_11,result_12,result_13,result_14,result_15,result_16 :  std_logic_vector(3 downto 0);

  
begin
    clk <= not (clk) after 1*period1;
    reset <= '1',
           '0' after 90ns;
    dut: top
    port map(clk      => clk  ,
             reset    => reset, 
             max_pooling_done=>max_pooling_done,
             result_1 => result_1,
             result_2 => result_2,
             result_3 => result_3,
             result_4 => result_4,
             result_5 => result_5,
             result_6 => result_6,
             result_7 => result_7,
             result_8 => result_8,
             result_9 => result_9,
             result_10=> result_10,
             result_11=> result_11,
             result_12=> result_12,
             result_13=> result_13,
             result_14=> result_14,
             result_15=> result_15,
             result_16=> result_16
            );

end Behavioral;







--library IEEE;
--use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

--entity tb is
--end tb;

--architecture Behavioral of tb is
--component convolve
--   port(
--         clk   : in std_logic;
--         reset   : in std_logic
--        );
--end component;
--     constant period1    : time := 10ns;
--     signal clk          : std_logic := '0';
--     signal reset          : std_logic;

  
--begin
--    clk <= not (clk) after 1*period1;
--    reset <= '1',
--           '0' after 90ns;
--    dut: convolve
--    port map(
--            clk     =>       clk  ,
--            reset     =>       reset  

--            );

--end Behavioral;



















