library ieee;
library work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.util_pkg.all;

entity tb_fully_connect_node is
end tb_fully_connect_node;

architecture structural of tb_fully_connect_node is
    component fully_connect_node is
        generic(
            INPUT_COUNT: natural;
            INPUT_SIZE: natural;
            WEIGHT_SIZE: natural := 4;
            BIAS_SIZE: natural := 6
        );
        port(
            clk, reset: in std_logic;
            parallel_load: in std_logic;
            input: in std_logic_vector(INPUT_COUNT * INPUT_SIZE - 1 downto 0);
            weights: in std_logic_vector(INPUT_COUNT * WEIGHT_SIZE - 1 downto 0);
            bias: in signed(BIAS_SIZE - 1 downto 0);
            output: out std_logic_vector(sum_size(INPUT_SIZE, WEIGHT_SIZE, INPUT_COUNT) - 1 downto 0)
        );
    end component;
    
    signal clk: std_logic := '0';
    signal reset: std_logic := '0';
    signal parallel_load: std_logic;
    signal input: std_logic_vector(63 downto 0);
    signal weights: std_logic_vector(63 downto 0);
    signal bias: signed(5 downto 0);
    signal output: std_logic_vector(13 downto 0);
    signal input_2: std_logic_vector(38 downto 0);
    signal weights_2: std_logic_vector(11 downto 0);
    signal bias_2: signed(5 downto 0);
    signal output_2: std_logic_vector(19 downto 0);
    
    constant period: time := 10 ns;
    
begin
    DUT_layer_1: fully_connect_node
        generic map(
            INPUT_COUNT => 16,
            INPUT_SIZE => 4
        )
        port map(
            clk => clk,
            reset => reset,
            parallel_load => parallel_load,
            input => input,
            weights => weights,
            bias => bias,
            output => output
        );
        
    DUT_layer_2: fully_connect_node
        generic map(
            INPUT_COUNT => 3,
            INPUT_SIZE => 13
        )
        port map(
            clk => clk,
            reset => reset,
            parallel_load => parallel_load,
            input => input_2,
            weights => weights_2,
            bias => bias_2,
            output => output_2
        );
        
    clk <= not clk after period/2;
    reset <= '1',
             '0' after period/2;
    parallel_load <= '1' after period,
                     '0' after 2*period;

    --5     7     3     1
    --5     8     9     1
    --6     0     2     4
    --1     3     0     5
    --Expected output: -52    
    input <= std_logic_vector(to_unsigned(5, 4)) & std_logic_vector(to_unsigned(7, 4)) & std_logic_vector(to_unsigned(3, 4)) & std_logic_vector(to_unsigned(1, 4)) &
             std_logic_vector(to_unsigned(5, 4)) & std_logic_vector(to_unsigned(8, 4)) & std_logic_vector(to_unsigned(9, 4)) & std_logic_vector(to_unsigned(1, 4)) & 
             std_logic_vector(to_unsigned(6, 4)) & std_logic_vector(to_unsigned(0, 4)) & std_logic_vector(to_unsigned(2, 4)) & std_logic_vector(to_unsigned(4, 4)) & 
             std_logic_vector(to_unsigned(1, 4)) & std_logic_vector(to_unsigned(3, 4)) & std_logic_vector(to_unsigned(0, 4)) & std_logic_vector(to_unsigned(5, 4));
    --Weights and bias are fc_filter2 from Matlab
    weights <= std_logic_vector(to_signed(3, 4)) & std_logic_vector(to_signed(-2, 4)) & std_logic_vector(to_signed(-2, 4)) & std_logic_vector(to_signed(0, 4)) &
               std_logic_vector(to_signed(-3, 4)) & std_logic_vector(to_signed(-1, 4)) & std_logic_vector(to_signed(-2, 4)) & std_logic_vector(to_signed(0, 4)) & 
               std_logic_vector(to_signed(-5, 4)) & std_logic_vector(to_signed(-2, 4)) & std_logic_vector(to_signed(1, 4)) & std_logic_vector(to_signed(-3, 4)) & 
               std_logic_vector(to_signed(-2, 4)) & std_logic_vector(to_signed(2, 4)) & std_logic_vector(to_signed(-6, 4)) & std_logic_vector(to_signed(2, 4));
    bias <= to_signed(20, 6);
    
    --510     0     5
    --Expected output: -4039
    input_2 <= std_logic_vector(to_unsigned(5, 13)) & std_logic_vector(to_unsigned(0, 13)) & std_logic_vector(to_unsigned(510, 13));
    --Weights and bias are fc_filter4 from Matlab
    weights_2 <= std_logic_vector(to_signed(7, 4)) & std_logic_vector(to_signed(7, 4)) & std_logic_vector(to_signed(-8, 4));
    bias_2 <= to_signed(6, 6);
end structural;